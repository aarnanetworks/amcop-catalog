# rootsyncAmcopOperator

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] rootsyncAmcopOperator`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree rootsyncAmcopOperator`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init rootsyncAmcopOperator
kpt live apply rootsyncAmcopOperator --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/

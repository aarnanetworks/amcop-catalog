# clusterbootstrapmgmt

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] clusterbootstrapmgmt`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree clusterbootstrapmgmt`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init clusterbootstrapmgmt
kpt live apply clusterbootstrapmgmt --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
